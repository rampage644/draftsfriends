<?php
/**
 *  Plugin Name: Drafts for Friends
 * Plugin URI: http://automattic.com/
 * Description: Now you don't need to add friends as users to the blog in order to let them preview your drafts
 * Author: Neville Longbottom
 * Version: 2.2
 * Author URI:
 *
 * @package DraftsForFriends
 */

/**
 * Drafts for friends
 */
class Drafts_For_Friends {
	/**
	 * Constructor
	 */
	public function __construct() {
		add_action( 'init', array( &$this, 'init' ) );
	}

	/**
	 * Initialize
	 */
	public function init() {
		global $current_user;
		add_action( 'admin_menu', array( $this, 'add_admin_pages' ) );
		add_filter( 'the_posts', array( $this, 'the_posts_intercept' ) );
		add_filter( 'posts_results', array( $this, 'posts_results_intercept' ) );

		$this->admin_options = $this->get_admin_options();

		$this->user_options = ( $current_user->ID > 0 && isset( $this->admin_options[ $current_user->ID ] ) ) ? $this->admin_options[ $current_user->ID ] : array();

		$this->save_admin_options();

		$this->admin_page_init();
	}
	/**
	 * Initialize admin page
	 */
	private function admin_page_init() {
		wp_enqueue_script( 'jquery' );
		wp_enqueue_script( 'drafts-for-friends', plugins_url( 'js/drafts-for-friends.js', __FILE__ ), array( 'jquery' ) );
		wp_enqueue_style( 'drafts-for-friends', plugins_url( 'css/drafts-for-friends.css', __FILE__ ) );
	}

	/**
	 * Gets admin options
	 *
	 * @return array of options
	 */
	private function get_admin_options() {
		$saved_options = get_option( 'shared' );
		return is_array( $saved_options ) ? $saved_options : array();
	}

	/**
	 * Updates admin options
	 */
	private function save_admin_options() {
		global $current_user;
		if ( $current_user->ID > 0 ) {
			$this->admin_options[ $current_user->ID ] = $this->user_options;
		}
		update_option( 'shared', $this->admin_options );
	}
	/**
	 * Adds submenu for admin page
	 */
	public function add_admin_pages() {
		add_submenu_page(
			'edit.php', __( 'Drafts for Friends', 'drafts-for-friends' ), __( 'Drafts for Friends', 'drafts-for-friends' ),
			'edit_posts', 'drafts-for-friends', array( $this, 'output_existing_menu_sub_admin_page' )
		);
	}

	/**
	 * Calculates expiration datetime
	 *
	 * @param array $params Input parameters.
	 */
	private function calc( $params ) {
		$exp      = 60;
		$multiply = 60;

		if ( isset( $params['expires'] ) ) {
			$exp = absint( $params['expires'] );
		}
		$mults = array(
			's' => 1,
			'm' => 60,
			'h' => 3600,
			'd' => 24 * 3600,
		);
		if ( $params['measure'] && $mults[ $params['measure'] ] ) {
			$multiply = $mults[ $params['measure'] ];
		}
		return $exp * $multiply;
	}

	/**
	 * Processes POST submit request
	 *
	 * @param array $params POST parameters.
	 */
	private function process_post_options( $params ) {
		global $current_user;
		if ( $params['post_id'] ) {
			$p = get_post( $params['post_id'] );
			if ( ! $p ) {
				return __( 'There is no such post!', 'drafts-for-friends' );
			}
			if ( 'publish' === get_post_status( $p ) ) {
				return __( 'The post is published!', 'drafts-for-friends' );
			}
			$this->user_options['shared'][] = array(
				'id'      => $p->ID,
				'expires' => time() + $this->calc( $params ),
				'key'     => 'baba_' . wp_generate_password( 8 ),
			);
			$this->save_admin_options();
		}
	}
	/**
	 * Processes POST delete request
	 *
	 * @param array $params POST request parameters.
	 */
	private function process_delete( $params ) {
		$shared = array();
		foreach ( $this->user_options['shared'] as $share ) {
			if ( $share['key'] === $params['key'] ) {
				continue;
			}
			$shared[] = $share;
		}
		$this->user_options['shared'] = $shared;
		$this->save_admin_options();
	}
	/**
	 * Processes POST extend request
	 *
	 * @param array $params POST request parameters.
	 */
	private function process_extend( $params ) {
		$shared = array();
		foreach ( $this->user_options['shared'] as $share ) {
			if ( $share['key'] === $params['key'] ) {
				$share['expires'] += $this->calc( $params );
			}
			$shared[] = $share;
		}
		$this->user_options['shared'] = $shared;
		$this->save_admin_options();
	}
	/**
	 * Gets user drafts
	 *
	 * @return array User drafts.
	 */
	private function get_drafts() {
		global $current_user;
		$my_drafts    = get_users_drafts( $current_user->ID );
		$my_scheduled = $this->get_users_future( $current_user->ID );
		$pending      = get_posts( $current_user->ID );
		$ds           = array(
			array(
				__( 'Your Drafts:', 'drafts-for-friends' ),
				count( $my_drafts ),
				$my_drafts,
			),
			array(
				__( 'Your Scheduled Posts:', 'drafts-for-friends' ),
				count( $my_scheduled ),
				$my_scheduled,
			),
			array(
				__( 'Pending Review:', 'drafts-for-friends' ),
				count( $pending ),
				$pending,
			),
		);
		return $ds;
	}
	/**
	 * Gets specific user future posts.
	 *
	 * @param string $user_id ID of the user.
	 */
	private function get_users_future( $user_id ) {
		$args  = array(
			'author'      => absint( $user_id ),
			'post_status' => 'future',
			'post_type'   => 'post',
			'orderby'     => 'modified',
		);
		$posts = new WP_Query( $args );

		$drafts = array();
		foreach ( $posts->posts as $the_post ) {
			$obj             = new stdClass();
			$obj->ID         = absint( $the_post->ID );
			$obj->post_title = $the_post->post_title;
			$drafts[]        = $obj;
		}
		return $drafts;
	}
	/**
	 * Gets shared posts
	 *
	 * @return array List of currently shared user posts.
	 */
	private function get_shared() {
		return $this->user_options['shared'];
	}
	/**
	 * Generates HTML <tr></>tr for a single share
	 *
	 * @param array $share Share object to build row for.
	 */
	private function row_for( $share ) {
		$p   = get_post( $share['id'] );
		$url = get_bloginfo( 'url' ) . '/?p=' . $p->ID . '&drafts-for-friends=' . $share['key'];

		?>
		<tr>
			<td><?php echo absint( $p->ID ); ?></td>
			<td><?php echo esc_html( $p->post_title ); ?></td>
			<td><a href="<?php echo esc_url( $url ); ?>"><?php echo esc_html( $url ); ?></a></td>
			<td class="actions">
				<a class="drafts-for-friends-extend edit" id="drafts-for-friends-extend-link-<?php echo esc_attr( $share['key'] ); ?>" data-key="<?php echo esc_attr( $share['key'] ); ?>" href="#"><?php esc_attr_e( 'Extend', 'drafts-for-friends' ); ?></a>
				<form class="drafts-for-friends-extend" data-key="<?php echo esc_attr( $share['key'] ); ?>" id="<?php echo esc_attr( 'drafts-for-friends-extend-form-' . $share['key'] ); ?>" method="post">

					<?php wp_nonce_field( 'process', 'process' ); ?>
					<input type="hidden" name="action" value="extend" />
					<input type="hidden" name="key" value="<?php echo esc_attr( $share['key'] ); ?>" />
					<input type="submit" class="button" name="drafts-for-friends_extend_submit"value="<?php esc_attr_e( 'Extend', 'drafts-for-friends' ); ?>"/>
						<?php esc_attr_e( 'by', 'drafts-for-friends' ); ?>
						<?php echo $this->tmpl_measure_select(); ?>
					<a class="drafts-for-friends-extend-cancel" href="#" data-key="<?php echo esc_attr( $share['key'] ); ?>">
						<?php esc_attr_e( 'Cancel', 'drafts-for-friends' ); ?>
					</a>
				</form>
			</td>
			<td class="actions">
				<a class="delete" href="<?php echo esc_url( $this->get_delete_url( $share ) ); ?>"><?php esc_html_e( 'Delete', 'drafts-for-friends' ); ?></a>
			</td>
			<td><?php echo esc_html( human_time_diff( $share['expires'] ), 'drafts-for-friends' ); ?></td>
		</tr>
		<?php
	}

	/**
	 * Checks nonce
	 *
	 * @param string $nonce nonce to check.
	 * @param string $action action to check against.
	 */
	private function check_nonce( $nonce, $action = 'process' ) {
		if ( ! wp_verify_nonce( $nonce, $action ) ) {
			die( esc_attr__( 'The nonce failed, and we couldn\'t go any further...', 'drafts-for-friends' ) );
		}
	}
	/**
	 * Generates delete url with nonce
	 *
	 * @param array $share The array with the key to delete.
	 * @return string $url delete URL.
	 *
	 */
	private function get_delete_url( $share ) {
		$delete_url = admin_url( 'edit.php' );
		$args       = array(
			'page'   => 'drafts-for-friends',
			'action' => 'delete',
			'key'    => esc_attr( $share['key'] ),
			'nonce'  => wp_create_nonce( 'delete' ),
		);
		$url        = add_query_arg( $args, $delete_url );
		return $url;
	}
	/**
	 * Outputs main page contents
	 */
	public function output_existing_menu_sub_admin_page() {
		if ( isset( $_POST['drafts-for-friends_submit'] ) ) {
			$this->check_nonce( $_POST['process'] );
			$t = $this->process_post_options( $_POST );
		} elseif ( isset( $_POST['action'] ) && 'extend' === $_POST['action'] ) {
			$this->check_nonce( $_POST['process'] );
			$t = $this->process_extend( $_POST );
		} elseif ( isset( $_GET['action'] ) && 'delete' === $_GET['action'] ) {
			$this->check_nonce( $_GET['nonce'], 'delete' );
			$t = $this->process_delete( $_GET );
		}
		$ds = $this->get_drafts();
		?>
		<div class="wrap">
		<h2><?php esc_html_e( 'Drafts for Friends', 'drafts-for-friends' ); ?></h2>
		<?php if ( isset( $t ) ) : ?>
			<div id="message" class="updated fade"><?php echo esc_html( $t ); ?></div>
		<?php endif; ?>
		<h3><?php esc_html_e( 'Currently shared drafts', 'drafts-for-friends' ); ?></h3>
		<table class="widefat">
			<thead>
				<tr>
					<th><?php esc_html_e( 'ID', 'drafts-for-friends' ); ?></th>
					<th><?php esc_html_e( 'Title', 'drafts-for-friends' ); ?></th>
					<th><?php esc_html_e( 'Link', 'drafts-for-friends' ); ?></th>
					<th colspan="2" class="actions"><?php esc_html_e( 'Actions', 'drafts-for-friends' ); ?></th>
					<th><?php esc_html_e( 'Expires in', 'drafts-for-friends' ); ?></th>
				</tr>
			</thead>
			<tbody>
		<?php
		$s    = $this->get_shared();
		$time = time();

		// Check if already expired, delete then.
		foreach ( $s as $share ) :
			if ( $share['expires'] <= $time ) {
				$this->process_delete( $share );
				continue;
			}
		endforeach;

		foreach ( $s as $share ) :
			$this->row_for( $share );
		endforeach;

		if ( empty( $s ) ) :
		?>
			<tr>
				<td colspan="5"><?php esc_html_e( 'No shared drafts!', 'drafts-for-friends' ); ?></td>
			</tr>
		<?php
		endif;

		?>
			</tbody>
		</table>

		<h3><?php esc_html_e( 'Drafts for Friends', 'drafts-for-friends' ); ?></h3>
		<form id="drafts-for-friends-share" action=""            method="post">
			<p>
				<select id="drafts-for-friends-postid"     name="post_id">
					<option value=""><?php esc_html_e( 'Choose a draft', 'drafts-for-friends' ); ?></option>
					<?php
					foreach ( $ds as $dt ) :
						if ( $dt[1] ) :
					?>
						<option value="" disabled="disabled"></option>
						<option value="" disabled="disabled"><?php echo esc_html( $dt[0] ); ?></option>
						<?php
						foreach ( $dt[2] as $d ) :
							if ( empty( $d->post_title ) ) {
								continue;
							}
							?>
							<option value="<?php echo esc_attr( $d->ID ); ?>"><?php echo esc_html( $d->post_title ); ?></option>
							<?php
						endforeach;
						endif;
					endforeach;
					?>
				</select>
			</p>
			<p>
				<?php wp_nonce_field( 'process', 'process' ); ?>
				<input type="submit" class="button" name="drafts-for-friends_submit" value="<?php esc_html_e( 'Share it', 'drafts-for-friends' ); ?>" />
				<?php esc_html_e( 'for', 'drafts-for-friends' ); ?>
				<?php echo $this->tmpl_measure_select(); ?>.
			</p>
		</form>
		</div>
		<?php
	}
	/**
	 * Checks if share could be accessed
	 *
	 * @param string $pid Post ID to check against.
	 * @return bool True if user can view the post, False otherwise.
	 */
	private function can_view( $pid ) {
		foreach ( $this->admin_options as $option ) {
			if ( empty( $option['shared'] ) ) {
				continue;
			}
			$shares = $option['shared'];
			foreach ( $shares as $share ) {
				if ( $share['key'] === $_GET['drafts-for-friends'] && $pid ) {
					return true;
				}
			}
		}
		return false;
	}
	/**
	 * Shows the post if is not pulbished and is viewable
	 *
	 * @param array $pp List of posts to filter
	 *
	 * @return array Accessible posts
	 */
	public function posts_results_intercept( $pp ) {
		if ( 1 !== count( $pp ) ) {
			return $pp;
		}
		$p      = $pp[0];
		$status = get_post_status( $p );
		if ( 'publish' !== $status && $this->can_view( $p->ID ) ) {
			$this->shared_post = $p;
		}
		return $pp;
	}
	/**
	 * Sets shared_post var
	 *
	 * @param array $pp List of posts.
	 *
	 * @return array Same posts as input.
	 */
	public function the_posts_intercept( $pp ) {
		if ( empty( $pp ) && ! is_null( $this->shared_post ) ) {
			return array( $this->shared_post );
		} else {
			$this->shared_post = null;
			return $pp;
		}
	}
	/**
	 * Outputs dropbox with time options and input field
	 *
	 * @return string
	 */
	private function tmpl_measure_select() {
		$secs  = __( 'seconds', 'drafts-for-friends' );
		$mins  = __( 'minutes', 'drafts-for-friends' );
		$hours = __( 'hours', 'drafts-for-friends' );
		$days  = __( 'days', 'drafts-for-friends' );
		return <<<SELECT
			<input name="expires" type="text" value="2" size="4"/>
			<select name="measure">
				<option value="s">$secs</option>
				<option value="m">$mins</option>
				<option value="h" selected="selected">$hours</option>
				<option value="d">$days</option>
			</select>
SELECT;
	}
}

new Drafts_For_Friends();
