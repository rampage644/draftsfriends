jQuery( document ).ready( function( $ ) {
    $( 'form.drafts-for-friends-extend' ).hide();
    $( 'a.drafts-for-friends-extend' ).show();
    $( 'a.drafts-for-friends-extend-cancel' ).css( 'display', 'inline' );


	$( document ).on( 'click', '.drafts-for-friends-extend', function( e ) {
        key = $( this ).data('key');

        $( this ).hide();
		$( 'form[data-key="' + key + '"]' ).show();
	});

	$( document ).on( 'click', '.drafts-for-friends-extend-cancel', function( e ) {
        key = $( this ).data('key');
		ret = $( 'form[data-key="' + key + '"]' ).hide();


        $( '.drafts-for-friends-extend[data-key="' + key + '"]' ).show();
    });

});
